/*
 * Vector.h
 *
 *  Created on: Jun 26, 2020
 *      Author: consultit
 */

#ifndef GEOMETRY_VECTOR_H_
#define GEOMETRY_VECTOR_H_

#include "Direction.h"
#include "Magnitude.h"

namespace gm
{

class Vector
{
public:
	Vector(const Direction &direction, const Magnitude &magnitude) :
			mDirection{ direction }, mMagnitude{ magnitude }
	{
		// TODO Auto-generated constructor stub

	}
	virtual ~Vector()
	{
		// TODO Auto-generated destructor stub
	}
	Vector(const Vector &other) = delete;
	Vector(Vector &&other) = delete;
	Vector& operator=(const Vector &other) = delete;
	Vector& operator=(Vector &&other) = delete;

	const Direction& getDirection() const
	{
		return mDirection;
	}

	const Magnitude& getMagnitude() const
	{
		return mMagnitude;
	}

private:
	Direction mDirection;
	Magnitude mMagnitude;
};

} /* namespace gm */

#endif /* GEOMETRY_VECTOR_H_ */
