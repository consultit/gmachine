/*
 * Position.h
 *
 *  Created on: Jun 26, 2020
 *      Author: consultit
 */

#ifndef GEOMETRY_POSITION_H_
#define GEOMETRY_POSITION_H_

namespace gm
{

class Position
{
public:
	Position()
	{
		// TODO Auto-generated constructor stub

	}
	virtual ~Position()
	{
		// TODO Auto-generated destructor stub
	}
	Position(const Position &other) = delete;
	Position(Position &&other) = delete;
	Position& operator=(const Position &other) = delete;
	Position& operator=(Position &&other) = delete;

};

} /* namespace gm */

#endif /* GEOMETRY_POSITION_H_ */
