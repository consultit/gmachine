/*
 * Point.h
 *
 *  Created on: Jun 26, 2020
 *      Author: consultit
 */

#ifndef GEOMETRY_POINT_H_
#define GEOMETRY_POINT_H_

#include "Position.h"

namespace gm
{

class Point
{
public:
	Point(const Position &position) :
			mPosition{ position }
	{
		// TODO Auto-generated constructor stub

	}
	virtual ~Point()
	{
		// TODO Auto-generated destructor stub
	}
	Point(const Point &other) = delete;
	Point(Point &&other) = delete;
	Point& operator=(const Point &other) = delete;
	Point& operator=(Point &&other) = delete;

	const Position& getPosition() const
	{
		return mPosition;
	}

private:
	Position mPosition;
};

} /* namespace gm */

#endif /* GEOMETRY_POINT_H_ */
