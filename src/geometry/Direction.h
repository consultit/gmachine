/*
 * Direction.h
 *
 *  Created on: Jun 26, 2020
 *      Author: consultit
 */

#ifndef GEOMETRY_DIRECTION_H_
#define GEOMETRY_DIRECTION_H_

namespace gm
{

class Direction
{
public:
	Direction()
	{
		// TODO Auto-generated constructor stub

	}
	virtual ~Direction()
	{
		// TODO Auto-generated destructor stub
	}
	Direction(const Direction &other) = delete;
	Direction(Direction &&other) = delete;
	Direction& operator=(const Direction &other) = delete;
	Direction& operator=(Direction &&other) = delete;

};

} /* namespace gm */

#endif /* GEOMETRY_DIRECTION_H_ */
