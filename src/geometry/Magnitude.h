/*
 * Magnitude.h
 *
 *  Created on: Jun 26, 2020
 *      Author: consultit
 */

#ifndef GEOMETRY_MAGNITUDE_H_
#define GEOMETRY_MAGNITUDE_H_

namespace gm
{

class Magnitude
{
public:
	Magnitude()
	{
		// TODO Auto-generated constructor stub

	}
	virtual ~Magnitude()
	{
		// TODO Auto-generated destructor stub
	}
	Magnitude(const Magnitude &other) = delete;
	Magnitude(Magnitude &&other) = delete;
	Magnitude& operator=(const Magnitude &other) = delete;
	Magnitude& operator=(Magnitude &&other) = delete;

};

} /* namespace gm */

#endif /* GEOMETRY_MAGNITUDE_H_ */
