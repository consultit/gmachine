/*
 * AffineSpace.h
 *
 *  Created on: Jun 26, 2020
 *      Author: consultit
 */

#ifndef GEOMETRY_AFFINESPACE_H_
#define GEOMETRY_AFFINESPACE_H_

#include <string>
#include "Point.h"
#include "Vector.h"

namespace gm
{

class AffineSpace
{
public:
	AffineSpace(const std::string &name, unsigned int dimension) :
			mName
			{ name }, mDimension
			{ dimension }
	{
		// TODO Auto-generated constructor stub

	}
	virtual ~AffineSpace()
	{
		// TODO Auto-generated destructor stub
	}
	AffineSpace(const AffineSpace &other) = delete;
	AffineSpace(AffineSpace &&other) = delete;
	AffineSpace& operator=(const AffineSpace &other) = delete;
	AffineSpace& operator=(AffineSpace &&other) = delete;

	unsigned int getDimension() const
	{
		return mDimension;
	}

	const std::string& getName() const
	{
		return mName;
	}

	Point getPoint(const Position &position) const
	{
		return Point(position);
	}

	Vector getVector(const Direction &direction,
			const Magnitude &magnitude) const
	{
		return Vector(direction, magnitude);
	}

private:
	std::string mName;
	unsigned int mDimension;
};

using Space = AffineSpace;

// Space ← SCreate( name:string, dim:integer)
inline Space SCreate(std::string &name, unsigned int dimension)
{
	return Space(name, dimension);
}

// Vector ← VVAdd( v, w : Vector)
inline Vector VVAdd(const Vector &v, const Vector &w)
{
	return v + w;
}

inline Vector operator +(const Vector &v, const Vector &w)
{
}

// Vector ← SVMult( s : Scalar; v : Vector)
inline Vector SVMult(const Vector &v, float s)
{
	return v * s;
}

inline Vector operator *(const Vector &v, float s)
{
}

// Vector ← PPDiff( p1, p2 : Point)
inline Vector PPDiff(const Point &p1, const Point &p2)
{
	return p1 - p2;
}

inline Vector operator -(const Point &p1, const Point &p2)
{
}

// Point ← PVAdd( p : Point; v : Vector)
inline Point PVAdd(const Point &p, const Vector &v)
{
	return p + v;
}

inline Point operator +(const Point &p, const Vector &v)
{
}

// Point ← PPac( P, Q : Point; a, b : Scalar)
inline Point PPac(const Point &P, const Point &Q, float a, float b)
{
	return P + (Q - P) * (b / (a + b));
}

} /* namespace gm */

#endif /* GEOMETRY_AFFINESPACE_H_ */
